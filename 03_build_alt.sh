#!/bin/bash
export CXX=g++-10 CC=gcc-10
NCPU="$(grep -c processor /proc/cpuinfo)"

pushd automata-library
./build.sh -d debug -m Debug -n
popd
