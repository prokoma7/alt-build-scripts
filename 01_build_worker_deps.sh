#!/bin/bash
export CXX=g++-10 CC=gcc-10
DEPS_INSTALL_PREFIX="$(pwd)/deps-prefix"
NCPU="$(grep -c processor /proc/cpuinfo)"

mkdir -p "$DEPS_INSTALL_PREFIX"

download () {
  git clone --depth 1 --branch "$3" "$2" && cd "$1"
}

download_and_build_with_cmake () {
  (
    download "$@" && \
    mkdir -p build && cd build && cmake .. -DCMAKE_INSTALL_PREFIX="$DEPS_INSTALL_PREFIX" && \
    make -j "$NCPU" && make install
  )
}

download_and_build_activemq () {
  (
    download "$@" && \
    cd activemq-cpp && AMQCPP_INSTALL_PREFIX="$DEPS_INSTALL_PREFIX" ./build.sh configure && \
    ./build.sh compile && ( cd build && make install )
  )
}

download_and_build_with_cmake argparse https://github.com/p-ranav/argparse master
download_and_build_with_cmake jsoncpp https://github.com/open-source-parsers/jsoncpp master
download_and_build_with_cmake Catch2 https://github.com/catchorg/Catch2.git v2.x
download_and_build_with_cmake spdlog https://github.com/gabime/spdlog v1.x
download_and_build_activemq activemq-cpp https://gitbox.apache.org/repos/asf/activemq-cpp.git activemq-cpp-3.9.5

