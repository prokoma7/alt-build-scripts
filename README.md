# ALT Build Scripts

A collection of scripts to build [Algorithms Library Toolkit](https://alt.fit.cvut.cz/) on Ubuntu 20.04 including webui.

## Initial build

```bash
# clone alt & webui
./00_clone.sh

# build deps not present in repos
./01_build_worker_deps.sh

# this requires JDK >= 16 and Node.js LTS
./02_build_server_webui.sh

# build the ALT itself, this can take a long while
./03_build_alt.sh

# build the worker, this needs to be rebuild everytime ALT is modified
./04_build_worker.sh

# run everything, the webui is at http://localhost:8000/
./05_run_server_client.sh &
sleep 2
./06_run_worker.sh
```

## Incremental build (after modification of ALT)

```bash
./03_build_alt.sh
./04_build_worker.sh
./06_run_worker.sh
```
