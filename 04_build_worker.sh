#!/bin/bash
export CXX=g++-10 CC=gcc-10
ALT_INSTALL_PREFIX="$(pwd)/automata-library/debug/install"
DEPS_INSTALL_PREFIX="$(pwd)/deps-prefix"
NCPU="$(grep -c processor /proc/cpuinfo)"

pushd webui-client/worker
mkdir -p build && cd build && cmake .. -DCMAKE_PREFIX_PATH="$ALT_INSTALL_PREFIX;$DEPS_INSTALL_PREFIX" && make -j "$NCPU"
popd
