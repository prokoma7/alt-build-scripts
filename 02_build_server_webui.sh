#!/bin/bash

( cd webui-client/server && ./gradlew build )
( cd webui-client/webui && ln -sf .env.development .env.local && npm install --no-audit && npm run build )
